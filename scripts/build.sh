#!/bin/bash

_pwd=`pwd`
_envs=`pwd`/.env
_build_dir=`pwd`/build

mkdir -p $_build_dir

FRONTEND_DIMG_TAG=`grep FRONTEND_DIMG_TAG .env | cut -d'=' -f2`
BACKEND_DIMG_TAG=`grep BACKEND_DIMG_TAG .env | cut -d'=' -f2`
DOCKERHUB_USERNAME=`grep DOCKERHUB_USERNAME .env | cut -d'=' -f2`
DEFAULT_SERVER_URL=`grep SERVER_URL .env | cut -d'=' -f2`

echo $FRONTEND_DIMG_TAG
_BACKEND_DIMG_TAG=0.0.$((`echo ${BACKEND_DIMG_TAG} | cut -d'.' -f3` + 1))

cd ${_build_dir}
git clone git@bitbucket.org:malkbc268/emg.git || (cd emg && git pull)
cd emg
echo ${_BACKEND_DIMG_TAG}
docker build -t ${DOCKERHUB_USERNAME}/exhibition:${_BACKEND_DIMG_TAG} .
sed -i s/"BACKEND_DIMG_TAG=${BACKEND_DIMG_TAG}"/"BACKEND_DIMG_TAG=${_BACKEND_DIMG_TAG}"/ ${_envs}


cd $_build_dir
git clone git@bitbucket.org:malkbc268/frontend.git || (cd frontend && git pull)
cd frontend
_FRONTEND_DIMG_TAG=0.0.$((`echo ${FRONTEND_DIMG_TAG} | cut -d'.' -f3` + 1))
docker build --build-arg SERVER_URL=${SERVER_URL:-$DEFAULT_SERVER_URL} -t ${DOCKERHUB_USERNAME}/exhibition-frontend:${_FRONTEND_DIMG_TAG} .
sed -i s/"FRONTEND_DIMG_TAG=${FRONTEND_DIMG_TAG}"/"FRONTEND_DIMG_TAG=${_FRONTEND_DIMG_TAG}"/ ${_envs}


cd ${_pwd}/
# docker-compose pull
# bin/docker-compose config
# bin/docker-compose push
